package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public class Boot extends Equipment {

    private int shoesSize;

    public Boot(int shoesSize) {
        this.shoesSize = shoesSize;
    }

    public int getShoesSize() {
        return shoesSize;
    }

    @Override public String toString() {
        return "Boot{" +
                "shoesSize=" + shoesSize +
                ", price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Boot boot = (Boot) o;
        return shoesSize == boot.shoesSize;
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), shoesSize);
    }
}
