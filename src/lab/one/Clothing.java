package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public abstract class Clothing extends Equipment {

    protected int wearSize;

    public Clothing(int wearSize) {
        this.wearSize = wearSize;
    }

    public int getWearSize() {
        return wearSize;
    }

    public void setWearSize(int wearSize) {
        this.wearSize = wearSize;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Clothing clothing = (Clothing) o;
        return wearSize == clothing.wearSize;
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), wearSize);
    }
}
