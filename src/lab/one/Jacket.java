package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public class Jacket extends Clothing {

    private String jacketMaterial;

    public Jacket(int wearSize, String jacketMaterial) {
        super(wearSize);
        this.jacketMaterial = jacketMaterial;
    }

    public String getJacketMaterial() {
        return jacketMaterial;
    }

    public void setJacketMaterial(String jacketMaterial) {
        this.jacketMaterial = jacketMaterial;
    }

    @Override public String toString() {
        return "Jacket{" +
                "jacketMaterial='" + jacketMaterial + '\'' +
                ", wearSize=" + wearSize +
                ", price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Jacket jacket = (Jacket) o;
        return Objects.equals(jacketMaterial, jacket.jacketMaterial);
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), jacketMaterial);
    }

}
