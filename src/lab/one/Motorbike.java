package lab.one;

/**
 * Copyright © 2020. All rights reserved.
 *
 * @author Vadim
 */
public class Motorbike {

    private String brandName;
    private int housePower;

    public Motorbike(String brandName, int housePower) {
        this.brandName = brandName;
        this.housePower = housePower;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public int getHousePower() {
        return housePower;
    }

    public void setHousePower(int housePower) {
        this.housePower = housePower;
    }
}
