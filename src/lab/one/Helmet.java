package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public class Helmet extends Equipment {

    private int headSize;

    public Helmet(int headSize) {
        this.headSize = headSize;
    }

    public int getHeadSize() {
        return headSize;
    }

    public void setHeadSize(int headSize) {
        this.headSize = headSize;
    }

    @Override public String toString() {
        return "Helmet{" +
                "headSize=" + headSize +
                ", price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Helmet helmet = (Helmet) o;
        return headSize == helmet.headSize;
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), headSize);
    }
}
