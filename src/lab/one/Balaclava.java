package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
class Balaclava extends Clothing {

    private String color;


    public Balaclava(int wearSize, String color) {
        super(wearSize);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override public String toString() {
        return "Balaclava{" +
                "color='" + color + '\'' +
                ", wearSize=" + wearSize +
                ", price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Balaclava balaclava = (Balaclava) o;
        return Objects.equals(color, balaclava.color);
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }
}
