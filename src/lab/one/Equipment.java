package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020. All rights reserved.
 *
 * @author Vadim
 */
public abstract class Equipment {

    protected int price;
    protected int weight;
    protected String brandName;

    public Equipment() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    @Override public String toString() {
        return "Equipment{" +
                "price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equipment equipment = (Equipment) o;
        return price == equipment.price &&
                weight == equipment.weight &&
                Objects.equals(brandName, equipment.brandName);
    }

    @Override public int hashCode() {
        return Objects.hash(price, weight, brandName);
    }
}
