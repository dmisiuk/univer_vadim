package lab.one;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public class Glove extends Equipment {

    @Override public String toString() {
        return "Glove{" +
                "price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }
}
