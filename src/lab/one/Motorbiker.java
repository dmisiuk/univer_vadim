package lab.one;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright © 2020. All rights reserved.
 *
 * @author Vadim
 */
public class Motorbiker {

    private String name;
    private int age;
    private String country;

    private Motorbike motorbike;

    private List<Clothing> clothing = new ArrayList<>();

    private Helmet helmet;
    private Boot boot;
    private Glove glove;

    public Motorbiker(String name, int age, String country) {
        this.name = name;
        this.age = age;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Motorbike getMotorbike() {
        return motorbike;
    }

    public void setMotorbike(Motorbike motorbike) {
        this.motorbike = motorbike;
    }

    public List<Clothing> getClothing() {
        return clothing;
    }

    public void setClothing(List<Clothing> clothing) {
        this.clothing = clothing;
    }

    public Helmet getHelmet() {
        return helmet;
    }

    public void setHelmet(Helmet helmet) {
        this.helmet = helmet;
    }

    public Boot getBoot() {
        return boot;
    }

    public void setBoot(Boot boot) {
        this.boot = boot;
    }

    public Glove getGlove() {
        return glove;
    }

    public void setGlove(Glove glove) {
        this.glove = glove;
    }

    public List<Equipment> getEquipment() {
        List<Equipment> equipmentList = new ArrayList<Equipment>(clothing);
        if (helmet != null) {
            equipmentList.add(helmet);
        }
        if (glove != null) {
            equipmentList.add(glove);
        }

        if (boot != null) {
            equipmentList.add(boot);
        }
        return equipmentList;
    }
}
