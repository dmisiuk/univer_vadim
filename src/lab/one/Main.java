package lab.one;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
public class Main {

    public static void main(String[] args) {

        Motorbiker motorbiker = createMotoBiker();

        // calculate price
        int equipmentPrice = calculateEquipmentPrice(motorbiker);
        System.out.println("-------------- Equipment price = " + equipmentPrice);

        // sort equipment
        List<Equipment> equipment = motorbiker.getEquipment();
        sortEquipmentByWeight(equipment);
        System.out.println("-------------- Sorted equipment by weight:");
        for (int i = 0; i < equipment.size(); i++) {
            System.out.println(equipment.get(i));
        }

        // Find equipment elements in price range
        List<Equipment> equipmentInRange = findEquipmentInPriceRange(100, 300, motorbiker.getEquipment());
        System.out.println("-------------- Equipment with price between 100 and 300 :");
        for (int i = 0; i < equipmentInRange.size(); i++) {
            System.out.println(equipmentInRange.get(i));
        }
    }

    private static List<Equipment> findEquipmentInPriceRange(int minPrice, int maxPrice, List<Equipment> equipment) {
        List<Equipment> equipmentInRange = new ArrayList<Equipment>();
        for (int i = 0; i < equipment.size(); i++) {
            int price = equipment.get(i).price;
            if (price >= minPrice && price <= maxPrice) {
                equipmentInRange.add(equipment.get(i));
            }
        }

        return equipmentInRange;
    }

    private static void sortEquipmentByWeight(List<Equipment> equipment) {
        equipment.sort(new Comparator<Equipment>() {
            @Override
            public int compare(Equipment o1, Equipment o2) {
                return o1.getWeight() - o2.getWeight();
            }
        });
    }

    private static int calculateEquipmentPrice(Motorbiker motorbiker) {
        List<Equipment> equipmentList = motorbiker.getEquipment();
        int resultPrice = 0;
        for (int i = 0; i < equipmentList.size(); i++) {
            resultPrice += equipmentList.get(i).getPrice();
        }
        return resultPrice;
    }

    private static Motorbiker createMotoBiker() {
        Motorbiker motorbiker = new Motorbiker("Vadim", 20, "Belarus");
        Motorbike bike = new Motorbike("honda", 120);
        motorbiker.setMotorbike(bike);

        // creating Helmet
        Helmet helmet = new Helmet(8);
        helmet.setBrandName("Abibas");
        helmet.setPrice(200);
        helmet.setWeight(730);
        motorbiker.setHelmet(helmet);

        // creating Boot
        Boot boot = new Boot(42);
        boot.setBrandName("Ribak");
        boot.setPrice(130);
        boot.setWeight(800);
        motorbiker.setBoot(boot);

        // creating glove
        Glove glove = new Glove();
        glove.setBrandName("yamaha");
        glove.setPrice(30);
        glove.setWeight(103);
        motorbiker.setGlove(glove);

        // creating Balaclava
        Balaclava balaclava = new Balaclava(48, "black");
        balaclava.setBrandName("yamaha");
        balaclava.setPrice(15);
        balaclava.setWeight(100);
        motorbiker.getClothing().add(balaclava);

        // creating Jacket
        Jacket jacket = new Jacket(48, "Leather");
        jacket.setBrandName("Harley Davidson");
        jacket.setPrice(330);
        jacket.setWeight(980);
        motorbiker.getClothing().add(jacket);

        // creating jeans
        Jeans jeans = new Jeans(48, "slim-tapered");
        jeans.setBrandName("Colins");
        jeans.setPrice(74);
        jeans.setWeight(230);
        motorbiker.getClothing().add(jeans);

        return motorbiker;
    }
}
