package lab.one;

import java.util.Objects;

/**
 * Copyright © 2020 All rights reserved.
 *
 * @author Vadim
 */
class Jeans extends Clothing {

    private String jeansType;

    public Jeans(int wearSize, String jeansType) {
        super(wearSize);
        this.jeansType = jeansType;
    }

    public String getJeansType() {
        return jeansType;
    }

    public void setJeansType(String jeansType) {
        this.jeansType = jeansType;
    }

    @Override public String toString() {
        return "Jeans{" +
                "jeansType='" + jeansType + '\'' +
                ", wearSize=" + wearSize +
                ", price=" + price +
                ", weight=" + weight +
                ", brandName='" + brandName + '\'' +
                '}';
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Jeans jeans = (Jeans) o;
        return Objects.equals(jeansType, jeans.jeansType);
    }

    @Override public int hashCode() {
        return Objects.hash(super.hashCode(), jeansType);
    }
}
